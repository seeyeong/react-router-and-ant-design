import React from 'react'
import { Link, useRouteMatch } from 'react-router-dom';

export default function NavLink({label, to, activeOnlyWhenExact}) {
    let match = useRouteMatch({
        path: to,
        exact: activeOnlyWhenExact,
    });

    return (
        <div className={match ? 'nav active' : 'nav'}>
            <Link to={to}>{label}</Link>
        </div>
    );
}
