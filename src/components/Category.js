import React from 'react'
import { useParams } from 'react-router';

export default function Category() {
    let {category} = useParams();
    return <h1>Category: {category}</h1>;
}
