import React, {useState, createContext, useContext} from 'react';
import {BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom';
import Main from './components/Main';
import LoginPage from './components/LoginPage';

import 'antd/dist/antd.css';
import './App.css';

const authContext = createContext();

function useAuth() {
    return useContext(authContext);
}

function PrivateRoute({children, ...rest}) {
    let auth = useAuth();
    return <Route {...rest} render={({location}) => (auth.auth ? children : <Redirect to={{pathname: '/login', state: {from: location}}} />)} />;
}

function ProviderAuth({children}) {
    const auth = useProvideAuth();
    return <authContext.Provider value={auth}>{children}</authContext.Provider>;
}

function useProvideAuth() {
    const [auth, setAuth] = useState(null);

    const signin = (cb) => {
        return fakeAuth.signin(() => {
            setAuth('user');
            cb();
        });
    };

    const signout = () => {
        return fakeAuth.signout(() => {
            setAuth('');
        });
    };

    return {
        auth,
        signin,
        signout,
    };
}

const fakeAuth = {
    isLoggedin: false,
    signin(cb) {
        fakeAuth.isLoggedin = true;
        setTimeout(cb, 100);
    },
    signout(cb) {
        fakeAuth.isLoggedin = false;
        setTimeout(cb, 100);
    },
};

function App() {
    return (
        <ProviderAuth>
            <Router>
                <Switch>
                    <Route path="/login">
                        <LoginPage authContext={authContext} />
                    </Route>
                    <PrivateRoute path="/">
                        <Main authContext={authContext} />
                    </PrivateRoute>
                </Switch>
            </Router>
        </ProviderAuth>
    );
}

export default App;
