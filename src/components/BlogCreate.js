import React, {useState} from 'react'
import { Prompt } from 'react-router';
import {Form, Input, Button} from 'antd'

const {TextArea} = Input;

export default function BlogCreate() {
	const [isBlocking, setIsBlocking] = useState(false)
	const [form] = Form.useForm();

	const onFinish = (values) => {
		console.log("form values", values);
		setIsBlocking(false);
		form.resetFields();
	}

	return (
		<div>
			<Form form={form} onFinish={onFinish} >
				<Prompt when={isBlocking} message={location => `Are you sure want to go ${location.pathname}`} />
				<p>
					Blocking?
					{isBlocking ? "Yes, click a link or the back button": "Nope"}
				</p>
				<Form.Item name="title">
					<Input placeholder="Title" onChange={(e)=>setIsBlocking(e.target.value.length > 0)}/>
				</Form.Item>
				<Form.Item name="blog">
					<TextArea rows={10} onChange={(e)=>setIsBlocking(e.target.value.length > 0)}/>
				</Form.Item>
				<Button type="primary" htmlType="submit" block>Create</Button>
			</Form>
		</div>
	)
}
