import React, {useContext} from 'react';
import { useHistory, useLocation } from 'react-router';
import { Card, Form, Button, Input } from 'antd';

export default function LoginPage({authContext}) {
	const auth = useContext(authContext)
    let history = useHistory();
    let location = useLocation();
    let {from} = location.state || {from: {pathname: '/'}};
    let login = () => {
        auth.signin(() => {
            history.replace(from);
        });
    };
    return (
        <div className="login-layout">
			<Card style={{width: 300, margin: "0 auto", marginTop: "15%"}}>
				<Form>
					<Form.Item>
						<Input placeholder="Username" value="seeyeong"/>
					</Form.Item>
					<Form.Item>
						<Input.Password placeholder="Password" value="123123"/>
					</Form.Item>
					<Button type="primary" block onClick={login}>Login</Button>
				</Form>
			</Card>
        </div>
    );
}
