import React, { createContext, useContext } from 'react';
import {useRouteMatch, Switch, Route} from 'react-router-dom';
import { Alert, Layout, Menu } from 'antd';
import NavLink from './Navlink';
import BlogCreate from './BlogCreate';
import Category from './Category';

const {Sider, Content} = Layout;
const {SubMenu} = Menu;

function Home() {
    return <h1>This is Home</h1>;
}

function Blog() {
    let match = useRouteMatch();

    return (
        <div>
            <h1>Our Blog</h1>
			<Switch>
					<Route path={`${match.path}/create`} exact>
                        <BlogCreate />
                    </Route>
                    <Route path={`${match.path}/:category`}>
                        <Category />
                    </Route>
                    <Route path={match.path}>
                        <h3>Please select a topic.</h3>
                    </Route>
			</Switch>
        </div>
    );
}

function About() {
    return <h1>Our About</h1>;
}

export default function Main({authContext}) {
	const auth = useContext(authContext)
    return (
        <Layout className="main-layout">
            {/* Side Menu */}
            <Sider>
                <div className="logo" />
                <Menu theme="dark" mode="inline">
                    <Menu.Item>
                        <NavLink to="/home" label="Dashboard" />
                    </Menu.Item>
					<SubMenu key="blog" title="Blog">
						<Menu.ItemGroup >
							<Menu.Item>
								<NavLink to="/blog" label="Blog list" />
							</Menu.Item>
							<Menu.Item>
								<NavLink to="/blog/create" label="Create Blog" />
							</Menu.Item>
						</Menu.ItemGroup>
					</SubMenu>
                    <Menu.Item>
                        <NavLink to="/about" label="About" />
                    </Menu.Item>
                    <Menu.Item onClick={() => auth.signout()}>
                        Logout
                    </Menu.Item>
                </Menu>
            </Sider>

            {/* Content Layout*/}
            <Layout className="site-layout">
                {/* Content */}
                <Content className="site-layout-content site-layout-background">
                    {/* <h1>Welcome to Dashboard</h1> */}
                    <Switch>
                        <Route path="/home">
                            <Home />
                        </Route>
                        <Route path="/blog">
                            <Blog />
                        </Route>
                        <Route path="/about">
                            <About />
                        </Route>
                    </Switch>
                </Content>
            </Layout>
        </Layout>
    );
}